'use strict'
const { ACTION_CREATE, ACTION_UPDATE, ACTION_DESTROY } = require('./auditlog.model')

//
// Persoon (docent)
//
module.exports = (sequelize, DataTypes) => {
  const Person = sequelize.define(
    'Person',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      firstName: {
        allowNull: false,
        type: DataTypes.STRING(32)
      },

      lastName: {
        allowNull: false,
        type: DataTypes.STRING(64)
      },

      emailAdress: {
        allowNull: false,
        type: DataTypes.STRING(128),
        validate: {
          isEmail: true
        }
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  Person.associate = function(models) {
    models.Person.hasOne(models.Course, {
      foreignKey: 'responsiblePersonId',
      as: 'responsibleForCourse',
      constraints: false
    }),
      models.Person.hasMany(models.Module, {
        foreignKey: 'id',
        as: 'responsibleForModules',
        constraints: false
      }),
      models.Person.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return Person
}
