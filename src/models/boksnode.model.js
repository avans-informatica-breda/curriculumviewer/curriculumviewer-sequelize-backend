'use strict'

//
// BoksNode
//
module.exports = (sequelize, DataTypes) => {
  const BoksNode = sequelize.define(
    'BoksNode',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      parentId: {
        allowNull: true,
        type: DataTypes.INTEGER
      },

      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: 'A name is required.'
          }
        }
      },

      shortDescription: {
        type: DataTypes.STRING(400),
        allowNull: false,
        validate: {
          notNull: {
            msg: 'A Bloom taxonomy level is required.'
          }
        }
      },

      longDescription: {
        allowNull: true,
        type: DataTypes.TEXT
      },

      administrativeRemark: {
        allowNull: true,
        type: DataTypes.STRING
      },

      isLearningLine: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  BoksNode.associate = function(models) {
    models.BoksNode.belongsTo(models.BoksNode, {
      as: 'parent',
      constraints: false
    })

    models.BoksNode.hasMany(models.BoksNode, {
      foreignKey: 'parentId',
      as: 'childNodes',
      constraints: false
    })

    models.BoksNode.hasMany(models.BoksContent, {
      foreignKey: 'parentId',
      as: 'contentNodes',
      constraints: false
    })

    //
    // Leerlijn behoort tot 1 of meer leerdoelen.
    //
    models.BoksNode.belongsToMany(models.Objective, {
      as: 'objectives',
      through: {
        model: 'ObjectiveBoksNode',
        unique: false
      },
      foreignKey: 'boksnodeId'
      // constraints: true
    }),
      models.BoksNode.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return BoksNode
}
