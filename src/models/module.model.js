'use strict'

//
// Onderwijseenheid (module of vak)
//
module.exports = (sequelize, DataTypes) => {
  const Module = sequelize.define(
    'Module',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      name: {
        allowNull: false,
        type: DataTypes.STRING,
        validate: {
          notNull: {
            msg: 'A name is required'
          }
        }
      },

      shortDescription: {
        allowNull: false,
        type: DataTypes.STRING(400),
        validate: {
          notNull: {
            msg: 'A short description is required'
          }
        }
      },

      longDescription: {
        allowNull: true,
        type: DataTypes.TEXT
      },

      administrativeRemark: {
        allowNull: true,
        type: DataTypes.STRING
      },

      // Regulier of verkort
      form: {
        type: DataTypes.ENUM('Verkort', 'Regulier'),
        default: 'Regulier'
      },

      // ECs
      credits: {
        allowNull: false,
        type: DataTypes.DECIMAL(2, 1),
        validate: {
          min: 0.0,
          max: 15.0,
          notNull: {
            msg: 'Credits (ECs) is required'
          }
        }
      },

      responsiblePersonId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      courseId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Courses',
          key: 'id'
        }
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  Module.associate = function(models) {
    models.Module.belongsTo(models.Course, {
      as: 'course',
      constraints: true
    }),
      models.Module.belongsTo(models.Person, {
        as: 'responsiblePerson',
        constraints: true
      }),
      models.Module.hasMany(models.Objective, {
        foreignKey: 'moduleId',
        as: 'objectives',
        constraints: true
      }),
      models.Module.hasMany(models.BoksContent, {
        as: 'boksContent',
        constraints: true
      }),
      models.Module.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return Module
}
