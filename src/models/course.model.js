'use strict'
const { ACTION_CREATE, ACTION_UPDATE, ACTION_DESTROY } = require('./auditlog.model')

//
// Course (periode)
//
module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define(
    'Course',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      name: {
        allowNull: false,
        type: DataTypes.STRING,
        validate: {
          notNull: {
            msg: 'A name is required'
          }
        }
      },

      shortDescription: {
        allowNull: false,
        type: DataTypes.STRING(400),
        validate: {
          notNull: {
            msg: 'A short description is required'
          }
        }
      },

      longDescription: {
        allowNull: true,
        type: DataTypes.TEXT
      },

      administrativeRemark: {
        allowNull: true,
        type: DataTypes.STRING
      },

      // Jaar 1, 2, 3 of 4
      studyYear: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 1,
          max: 4,
          notNull: {
            msg: 'studyYear is vereist en moet tussen 1 en 4 liggen.'
          }
        }
      },

      // Periode 1, 2, 3 of 4
      trimester: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 1,
          max: 4,
          notNull: {
            msg: 'trimester is vereist en moet tussen 1 en 4 liggen.'
          }
        }
      },

      // Regulier of verkort
      form: {
        allowNull: false,
        type: DataTypes.ENUM('Verkort', 'Regulier'),
        default: 'Regulier',
        validate: {
          notNull: {
            msg: 'Een onderwijsvorm is vereist.'
          }
        }
      },

      responsiblePersonId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      indexes: [
        {
          unique: true,
          fields: ['studyYear', 'trimester', 'name']
        }
      ],
      sequelize
    }
  )

  Course.associate = function(models) {
    models.Course.hasMany(models.Module, {
      foreignKey: 'courseId',
      as: 'modules'
      // constraints: false
    }),
      models.Course.belongsTo(models.Person, {
        as: 'responsiblePerson',
        constraints: true
      }),
      models.Course.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return Course
}
