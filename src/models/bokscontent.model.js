'use strict'

//
// Bokscontent element
//
module.exports = (sequelize, DataTypes) => {
  const BoksContent = sequelize.define(
    'BoksContent',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      parentId: {
        allowNull: true,
        type: DataTypes.INTEGER
      },

      name: {
        allowNull: false,
        type: DataTypes.STRING,
        validate: {
          notNull: {
            msg: 'A name is required'
          }
        }
      },

      shortDescription: {
        allowNull: false,
        type: DataTypes.STRING(400),
        validate: {
          notNull: {
            msg: 'A short description is required'
          }
        }
      },

      longDescription: {
        type: DataTypes.TEXT
      },

      administrativeRemark: {
        allowNull: true,
        type: DataTypes.STRING
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  BoksContent.associate = function(models) {
    models.BoksContent.belongsTo(models.BoksNode, {
      as: 'parent',
      constraints: false
    }),
      models.BoksContent.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return BoksContent
}
