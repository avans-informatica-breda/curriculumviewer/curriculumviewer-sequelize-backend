'use strict'

//
// Leerdoel
//
module.exports = (sequelize, DataTypes) => {
  const Ecf = sequelize.define('Ecf', {
    id: {
      allowNull: false,
      // autoIncrement: false,
      primaryKey: true,
      type: DataTypes.INTEGER
    },

    isEditable: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },

    areaTag: {
      allowNull: false,
      type: DataTypes.ENUM('A', 'B', 'C', 'D', 'E', 'F'),
      validate: {
        notNull: {
          msg: 'Een eCf areaTag is verplicht.'
        }
      }
    },

    areaName: {
      allowNull: false,
      type: DataTypes.ENUM('plan', 'build', 'run', 'enable', 'manage', 'professional skills'),
      validate: {
        notNull: {
          msg: 'Een eCf areaName is verplicht.'
        }
      }
    },

    competenceLevel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 15,
        notNull: {
          msg: 'Een competencelevel is verplicht.'
        }
      }
    },

    competenceName: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notNull: {
          msg: 'Een eCf areaName is verplicht.'
        }
      }
    },

    proficiencyLevel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 10,
        notNull: {
          msg: 'Een proficiencylevel is verplicht.'
        }
      }
    },

    proficiencySample: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notNull: {
          msg: 'Een proficiency sample beschrijving is verplicht.'
        }
      }
    }
  })

  Ecf.associate = function(models) {
    models.Ecf.belongsToMany(models.Objective, {
      as: 'coverage',
      through: {
        model: 'ObjectiveEcf',
        unique: false
      },
      foreignKey: 'ecfId'
    })
  }

  return Ecf
}
