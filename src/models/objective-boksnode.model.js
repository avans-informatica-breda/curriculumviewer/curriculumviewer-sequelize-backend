'use strict'

//
// Koppeltabel tussen Objective (leerdoel) en BoksNode (als leerlijn).
// Leerdoel heeft meerdere leerlijnen; leerlijn komt voor in meerdere leerdoelen.
//
module.exports = (sequelize, DataTypes) => {
  const ObjectiveBoksNode = sequelize.define(
    'ObjectiveBoksNode',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      objectiveId: {
        type: DataTypes.INTEGER,
        references: null
      },

      boksnodeId: {
        type: DataTypes.INTEGER,
        references: null
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  ObjectiveBoksNode.associate = function(models) {
    models.ObjectiveBoksNode.belongsTo(models.Person, {
      as: 'lastUpdatedBy',
      constraints: true
    })
  }

  return ObjectiveBoksNode
}
