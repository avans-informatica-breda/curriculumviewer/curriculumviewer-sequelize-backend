'use strict'

//
// Koppeltabel tussen Objective (leerdoel) en Ecf eindkwalificatie.
// Leerdoel heeft een of meer eindkwalificaties, die op een bepaald niveau behaald worden.
//
module.exports = (sequelize, DataTypes) => {
  const ObjectiveEcf = sequelize.define(
    'ObjectiveEcf',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      objectiveId: {
        type: DataTypes.INTEGER,
        references: null
      },

      ecfId: {
        type: DataTypes.INTEGER,
        references: null
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      sequelize
    }
  )

  ObjectiveEcf.associate = function(models) {
    models.ObjectiveEcf.belongsTo(models.Person, {
      as: 'lastUpdatedBy',
      constraints: true
    })
  }

  return ObjectiveEcf
}
