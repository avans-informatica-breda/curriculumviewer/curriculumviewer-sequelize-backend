var assert = require('assert')

// Define our constants
exports.ACTION_CREATE = 'create'
exports.ACTION_UPDATE = 'update'
exports.ACTION_DESTROY = 'destroy'
exports.VALID_ACTIONS = [exports.ACTION_CREATE, exports.ACTION_UPDATE, exports.ACTION_DESTROY]

// exports.SOURCE_USERS = 'users'
// exports.SOURCE_SERVER = 'server'
// exports.VALID_SOURCES = [exports.SOURCE_USERS, exports.SOURCE_SERVER]

module.exports = (sequelize, DataTypes) => {
  const AuditLog = sequelize.define(
    'AuditLog',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      transaction_id: {
        type: DataTypes.UUID,
        allowNull: true,
        validate: { isUUID: 'all' }
      },

      action: {
        type: DataTypes.STRING(32),
        allowNull: false,
        validate: {
          isIn: {
            args: [exports.VALID_ACTIONS],
            msg: `Action must be ${exports.ACTION_CREATE}, ${exports.ACTION_UPDATE} or ${
              exports.ACTION_DESTROY
            }.`
          }
        }
      },

      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      table_name: { type: DataTypes.STRING(64), allowNull: false },

      table_row_id: { type: DataTypes.INTEGER, allowNull: false },

      timestamp: { type: DataTypes.DATE, allowNull: false },

      previous_values: { type: DataTypes.JSON, allowNull: false },

      current_values: { type: DataTypes.JSON, allowNull: false }
    },
    {
      timestamps: false
    }
  )

  AuditLog.associate = function(models) {
    models.AuditLog.belongsTo(models.Person, {
      as: 'user',
      constraints: true
    })
  }

  return AuditLog
}
