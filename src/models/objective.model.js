'use strict'

//
// Leerdoel
//
module.exports = (sequelize, DataTypes) => {
  const Objective = sequelize.define(
    'Objective',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },

      isEditable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },

      year: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          min: 2000,
          max: 2030,
          notNull: {
            msg: 'year is vereist en moet groter dan 2000 zijn.'
          }
        }
      },

      lastUpdatedById: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'People',
          key: 'id',
          constraints: true
        }
      },

      name: {
        allowNull: false,
        type: DataTypes.STRING(400),
        validate: {
          len: [3, 256],
          notNull: {
            msg: 'A name is required.'
          }
        }
      },

      shortDescription: {
        allowNull: false,
        type: DataTypes.STRING(500),
        validate: {
          notNull: {
            msg: 'A short description is required.'
          }
        }
      },

      administrativeRemark: {
        allowNull: true,
        type: DataTypes.STRING
      },

      longDescription: {
        allowNull: true,
        type: DataTypes.TEXT
      },

      bloomTaxonomy: {
        allowNull: false,
        type: DataTypes.ENUM('Onthouden', 'Begrijpen', 'Toepassen', 'Analyseren', 'Evalueren', 'Creëren'),
        validate: {
          notNull: {
            msg: 'A Bloom taxonomy level is required.'
          }
        }
      },

      moduleId: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Modules',
          key: 'id',
          constraints: true
        }
      }
    },
    {
      hooks: {
        afterCreate: function(model, options) {
          return sequelize.saveAuditLog('create', model, options)
        },
        afterUpdate: function(model, options) {
          return sequelize.saveAuditLog('update', model, options)
        },
        afterDestroy: function(model, options) {
          return sequelize.saveAuditLog('destroy', model, options)
        }
      },
      // paranoid: true,
      sequelize
    }
  )

  Objective.associate = function(models) {
    //
    // Leerdoel wordt gerealiseert in 1 module (vak of onderwijseenheid)
    //
    models.Objective.belongsTo(models.Module, {
      as: 'module',
      constraints: true
    }),
      //
      // leerdoel draagt bij aan 0 of meer leerlijnen
      // Leerlijn realiseert een of meer leerdoelen
      //
      models.Objective.belongsToMany(models.BoksNode, {
        as: 'learningLines',
        through: 'ObjectiveBoksNode',
        foreignKey: 'objectiveId',
        onDelete: 'CASCADE'
      }),
      //
      // leerdoel draagt bij aan 0 of meer eindkwalificaties
      //
      models.Objective.belongsToMany(models.Ecf, {
        as: 'endqualifications',
        through: 'ObjectiveEcf',
        foreignKey: 'objectiveId',
        onDelete: 'CASCADE'
      }),
      //
      models.Objective.belongsTo(models.Person, {
        as: 'lastUpdatedBy',
        constraints: true
      })
  }

  return Objective
}
