'use strict'

const fs = require('fs')
const path = require('path')
const assert = require('assert')
const Sequelize = require('sequelize')
const basename = path.basename(__filename)
const env = process.env.NODE_ENV || 'development'
const config = require(__dirname + '/../config/config.js')[env]
const db = {}

// Use connection pooling
const pool = {
  max: 5,
  min: 0,
  acquire: 30000,
  idle: 10000
}

if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable], config, pool)
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config, pool)
}

fs.readdirSync(__dirname)
  .filter(file => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

db.sequelize.saveAuditLog = function(action, model, options) {
  assert(options.transaction, 'All create/update/destroy actions must be run in a transaction')

  var auditLog = db.AuditLog.build({
    table_name: model._modelOptions.name.singular,
    table_row_id: model.get('id'),
    action: action,
    userId: model.get('lastUpdatedById'),
    timestamp: new Date(),
    previous_values: model._previousDataValues,
    current_values: model.dataValues,
    transaction_id: options.transaction ? options.transaction.id : null
  })
  return auditLog.save({ transaction: options.transaction })
}

//
// Datasets that are more or less 'static'
//
const persons = [
  {
    firstName: 'Ruud',
    lastName: 'Hermans',
    lastUpdatedById: null,
    emailAdress: 'rl.hermans@avans.nl'
  },
  {
    firstName: 'Erco',
    lastName: 'Argante',
    lastUpdatedById: 1,
    emailAdress: 'e.argante@avans.nl'
  },
  {
    firstName: 'Arno',
    lastName: 'Broeders',
    lastUpdatedById: 2,
    emailAdress: 'ah.broeders@avans.nl'
  },
  {
    firstName: 'Robin',
    lastName: 'Schellius',
    lastUpdatedById: 2,
    emailAdress: 'r.schellius@avans.nl'
  }
]

const ecfValues = [
  {
    areaTag: 'A',
    areaName: 'Plan',
    competenceLevel: 5,
    competenceName: 'Architecture Design',
    proficiencyLevel: 1,
    lastUpdatedById: 2,
    proficiencySample: 'Sample proficiency description.'
  },
  {
    areaTag: 'A',
    areaName: 'Plan',
    competenceLevel: 5,
    competenceName: 'Architecture Design',
    proficiencyLevel: 2,
    lastUpdatedById: 2,
    proficiencySample: 'Sample proficiency description.'
  },
  {
    areaTag: 'A',
    areaName: 'Plan',
    competenceLevel: 5,
    competenceName: 'Architecture Design',
    proficiencyLevel: 3,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'A',
    areaName: 'Plan',
    competenceLevel: 6,
    competenceName: 'Application Design',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'A',
    areaName: 'Plan',
    competenceLevel: 8,
    competenceName: 'Sustainable Development',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 1,
    competenceName: 'Application Development',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 2,
    competenceName: 'Component Integration',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 3,
    competenceName: 'Testing',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 4,
    competenceName: 'Solution Deployment',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 5,
    competenceName: 'Documentation Production',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'B',
    areaName: 'Build',
    competenceLevel: 6,
    competenceName: 'Systems Engineering',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'C',
    areaName: 'Run',
    competenceLevel: 4,
    competenceName: 'Problem Management',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'D',
    areaName: 'Enable',
    competenceLevel: 11,
    competenceName: 'Needs Identification',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'E',
    areaName: 'Manage',
    competenceLevel: 2,
    competenceName: 'Project and Portfolio Management',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'F',
    areaName: 'Professional Skills',
    competenceLevel: 1,
    competenceName: 'Communicatieve Vaardigheden',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'F',
    areaName: 'Professional Skills',
    competenceLevel: 2,
    competenceName: 'Leervaardigheden',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'F',
    areaName: 'Professional Skills',
    competenceLevel: 3,
    competenceName: 'Oordeelsvorming & Onderzoekend Vermogen',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  },
  //
  //
  //
  {
    areaTag: 'F',
    areaName: 'Professional Skills',
    competenceLevel: 4,
    competenceName: 'Samenwerken & Omgevingsbewustzijn',
    proficiencyLevel: 1,
    proficiencySample: 'Sample proficiency description.'
  }
]

const boksNodes = [
  {
    name: 'Programmeren',
    shortDescription: 'Dit is de leerlijn Programmeren',
    administrativeRemark: 'test',
    isLearningLine: true,
    lastUpdatedById: 2,
    parentId: null
  },
  {
    name: 'Databases',
    shortDescription: 'Dit is de leerlijn Databases',
    childNodes: [],
    contentNodes: [],
    isLearningLine: true,
    longDescription: '',
    lastUpdatedById: 2,
    parentId: null
  },
  {
    name: 'Requirements Engineering',
    shortDescription: 'Dit is de leerlijn Requirements Engineering',
    // childNodes: [],
    // contentNodes: [],
    isLearningLine: true,
    // longDescription: '',
    lastUpdatedById: 2,
    parentId: null
  },
  {
    name: 'OO Programmeren',
    shortDescription: 'Dit is de BOKS node OO Programmeren.',
    // childNodes: [],
    // contentNodes: [],
    longDescription: 'Hier meer info over dit BOKS node element.',
    lastUpdatedById: 2,
    parentId: 1
  },
  {
    name: 'Functioneel Programmeren',
    shortDescription: 'Dit is de BOKS node Functioneel Programmeren.',
    // childNodes: [],
    // contentNodes: [],
    longDescription: 'Hier meer info over dit BOKS node element.',
    lastUpdatedById: 2,
    parentId: 1
  },
  {
    name: 'Relationele databases',
    shortDescription: 'Dit is de BOKS node Relationele databases.',
    // childNodes: [],
    // contentNodes: [],
    longDescription: 'Hier meer info over dit BOKS node element.',
    lastUpdatedById: 2,
    parentId: 2
  },
  {
    name: 'NoSql databases',
    shortDescription: 'Dit is de BOKS node NoSql databases.',
    lastUpdatedById: 2,
    // childNodes: [],
    // contentNodes: [],
    longDescription: 'Hier meer info over dit BOKS node element.',
    parentId: 2
  }
]

const boksContent = [
  {
    name: 'Threads',
    shortDescription: 'Dit is BOKSinhoud.',
    lastUpdatedById: 2,
    parentId: 3
  },
  {
    name: 'Methods',
    shortDescription: 'Dit is BOKSinhoud.',
    lastUpdatedById: 2,
    parentId: 3
  },
  {
    name: 'Overerving',
    shortDescription: 'Dit is BOKSinhoud.',
    lastUpdatedById: 2,
    parentId: 3
  }
]

const objectives = [
  {
    name: 'De student kan iets, zodat hij iets kan laten zien.',
    shortDescription: 'Hier kan aanvullende informatie staan',
    longDescription: 'Hier kan aanvullende informatie staan',
    bloomTaxonomy: 'Begrijpen',
    lastUpdatedById: 2,
    moduleId: 1,
    learningLines: [1],
    endqualifications: [1, 2]
  }
]

const courses = [
  {
    name: 'Basisvaardigheden 1',
    shortDescription: 'De eerste periode (course)',
    administrativeRemark: 'test',
    responsiblePersonId: 2,
    lastUpdatedById: 3,
    studyYear: 1,
    trimester: 1,
    lastUpdatedById: 2,
    form: 'Regulier'
  },
  {
    name: 'Basisvaardigheden 2',
    shortDescription: 'De tweede periode (course)',
    responsiblePersonId: 1,
    lastUpdatedById: 2,
    studyYear: 1,
    trimester: 2,
    form: 'Regulier'
  }
]

const modules = [
  {
    name: 'Programmeren 1',
    shortDescription: 'Hier een beschrijving van deze module',
    longDescription: 'Hier eventueel een uitgebreidere toelichting.',
    form: 'Regulier',
    courseId: 1,
    credits: 4.0,
    lastUpdatedById: 2,
    responsiblePersonId: 2
  },
  {
    name: 'Databases 1',
    shortDescription: 'Hier een beschrijving van deze module',
    longDescription: 'Hier eventueel een uitgebreidere toelichting.',
    form: 'Regulier',
    courseId: 1,
    credits: 4.0,
    lastUpdatedById: 2,
    responsiblePersonId: 1
  },
  {
    name: 'Webdesign',
    shortDescription: 'Hier een beschrijving van deze module',
    longDescription: 'Hier eventueel een uitgebreidere toelichting.',
    form: 'Regulier',
    courseId: 1,
    credits: 3.0,
    lastUpdatedById: 2,
    responsiblePersonId: 4
  },
  {
    name: 'Programmeren 2',
    shortDescription: 'Hier een beschrijving van deze module',
    longDescription: 'Hier eventueel een uitgebreidere toelichting.',
    form: 'Regulier',
    courseId: 2,
    credits: 4.0,
    lastUpdatedById: 2,
    responsiblePersonId: 2
  },
  {
    name: 'Relationele Databases 2',
    shortDescription: 'Hier een beschrijving van deze module',
    longDescription: 'Hier eventueel een uitgebreidere toelichting.',
    form: 'Regulier',
    courseId: 2,
    credits: 4.0,
    lastUpdatedById: 2,
    responsiblePersonId: 4
  }
]

//
// createStaticData
//
db['createStaticData'] = () => {
  db.sequelize.transaction(t => {
    persons.forEach(person =>
      db.Person.create(person)
        .then(result => {
          console.log(person.firstName + ' aangemaakt')
        })
        .catch(handleError)
    )

    ecfValues.forEach(item =>
      db.Ecf.create(item)
        .then(result => {
          console.log(`${item.areaTag}.${item.competenceLevel}.${item.proficiencyLevel} aangemaakt`)
        })
        .catch(handleError)
    )

    boksNodes.forEach(item =>
      db.BoksNode.create(item)
        .then(result => displayMessage(item))
        .catch(handleError)
    )

    boksContent.forEach(item =>
      db.BoksContent.create(item)
        .then(result => displayMessage(item))
        .catch(handleError)
    )

    courses.forEach(item =>
      db.Course.create(item)
        .then(result => displayMessage(item))
        .catch(handleError)
    )

    // modules
    //   .forEach(item =>
    //     db.Module.create(item, { transaction: t })
    //   )
    //   .then(result => displayMessage(result))
    //   .catch(handleError)

    objectives.forEach(item =>
      db.Objective.create(item)
        .then(result => result.setLearningLines(item.learningLines))
        .then(result => result.setEndqualifications(item.endqualifications))
        .then(displayMessage)
        .catch(handleError)
    )
  })
}

function displayMessage(item) {
  console.log(`${item.name} aangemaakt`)
}

function handleError(error) {
  // console.log('create error: ', error.toString())
}

module.exports = db
