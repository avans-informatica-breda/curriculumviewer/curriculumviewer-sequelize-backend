'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('Courses', 'editingUserId')
    return queryInterface.addColumn('Courses', 'lastUpdatedById', Sequelize.INTEGER)
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('Courses', 'lastUpdatedById')
    return queryInterface.addColumn('Courses', 'editingUserId', Sequelize.INTEGER)
  }
}
