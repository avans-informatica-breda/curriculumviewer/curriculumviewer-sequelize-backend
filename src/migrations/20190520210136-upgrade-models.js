'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn(
          'BoksNodes',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'BoksContents',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'Modules',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'ObjectiveBoksNodes',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'ObjectiveEcfs',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'Objectives',
          'year',
          {
            type: Sequelize.STRING
          },
          { transaction: t }
        )
      ])
    })
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn(
      'BoksNodes',
      'year',
      {
        type: Sequelize.STRING
      },
      { transaction: t }
    ),
      queryInterface.removeColumn(
        'BoksContents',
        'year',
        {
          type: Sequelize.STRING
        },
        { transaction: t }
      ),
      queryInterface.removeColumn(
        'Modules',
        'year',
        {
          type: Sequelize.STRING
        },
        { transaction: t }
      ),
      queryInterface.removeColumn(
        'ObjectiveBoksNodes',
        'year',
        {
          type: Sequelize.STRING
        },
        { transaction: t }
      ),
      queryInterface.removeColumn(
        'ObjectiveEcfs',
        'year',
        {
          type: Sequelize.STRING
        },
        { transaction: t }
      ),
      queryInterface.removeColumn(
        'Objectives',
        'year',
        {
          type: Sequelize.STRING
        },
        { transaction: t }
      )
  }
}
