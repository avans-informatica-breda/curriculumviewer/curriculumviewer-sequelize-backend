const express = require('express')
const bodyParser = require('body-parser')
const logger = require('./config/config').logger
const apiRoutes = require('./routes')
const HttpStatus = require('http-status')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// CORS headers
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', process.env.ALLOW_ORIGIN || 'http://localhost:4200')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

app.use((req, res, next) => {
  logger.info(`${req.method} ${req.originalUrl}`)
  next()
})

// Installing routes
app.use(apiRoutes)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = {
    message: `The requested ${req.method} endpoint '${req.originalUrl}' does not exist.`,
    code: HttpStatus.NOT_FOUND,
    date: new Date()
  }
  next(err)
})

app.use(function(err, req, res, next) {
  logger.error(err.toString() || err)
  res.status(err.code || HttpStatus.INTERNAL_SERVER_ERROR).json(err)
})

module.exports = app
