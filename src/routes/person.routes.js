const express = require('express')
const router = express.Router()
const PersonController = require('../controllers/person.controller')

router.post('/', PersonController.create)
router.get('/', PersonController.getAll)
router.get('/:personId', PersonController.getById)
router.put('/:personId', PersonController.updateById)
router.delete('/:personId', PersonController.deleteById)

// router.post('/:user_id/tasks/create', PersonController.createTask)
// router.get('/:user_id/tasks/:task_id/destroy', PersonController.deleteTask)

module.exports = router
