const express = require('express')
const router = express.Router()
const ModuleController = require('../controllers/module.controller')

router.post('/', ModuleController.create)
router.get('/', ModuleController.getAll)
router.get('/:moduleId', ModuleController.getById)
router.put('/:moduleId', ModuleController.updateById)
router.delete('/:moduleId', ModuleController.deleteById)

// router.post('/:user_id/tasks/create', ModuleController.createTask)
// router.get('/:user_id/tasks/:task_id/destroy', ModuleController.deleteTask)

module.exports = router
