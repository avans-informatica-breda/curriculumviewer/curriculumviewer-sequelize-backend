const express = require('express')
const router = express.Router()
const BoksNodeController = require('../controllers/boksnode.controller')
const BoksContentController = require('../controllers/bokscontent.controller')

router.get('/', BoksNodeController.getAll)
router.post('/', BoksNodeController.create)

router.get('/:boksnodeId', BoksNodeController.getById)
router.post('/:boksnodeId/subnode', BoksNodeController.createSubNode)
router.put('/:boksnodeId', BoksNodeController.updateById)
router.delete('/:boksnodeId', BoksNodeController.deleteById)

router.post('/:boksnodeId/content', BoksContentController.createContentNode)
router.put('/:boksnodeId/content/:bokscontentId', BoksContentController.updateById)
router.delete('/:boksnodeId/content/:bokscontentId', BoksContentController.deleteById)

module.exports = router
