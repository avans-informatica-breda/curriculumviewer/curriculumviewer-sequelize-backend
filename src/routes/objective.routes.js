const express = require('express')
const router = express.Router()
const ObjectiveController = require('../controllers/objective.controller')

router.post('/', ObjectiveController.create)
router.get('/', ObjectiveController.getAll)
router.get('/:objectiveId', ObjectiveController.getById)
router.put('/:objectiveId', ObjectiveController.updateById)
router.delete('/:objectiveId', ObjectiveController.deleteById)

// router.post('/:user_id/tasks/create', ObjectiveController.createTask)
// router.get('/:user_id/tasks/:task_id/destroy', ObjectiveController.deleteTask)

module.exports = router
