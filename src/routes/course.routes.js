const express = require('express')
const router = express.Router()
const CourseRoutes = require('../controllers/course.controller')

router.post('/', CourseRoutes.create)
router.get('/', CourseRoutes.getAll)
router.get('/:courseId', CourseRoutes.getById)
router.patch('/:courseId', CourseRoutes.updateById)
router.put('/:courseId', CourseRoutes.updateById)
router.delete('/:courseId', CourseRoutes.deleteById)

// router.post('/:user_id/tasks/create', CourseRoutes.createTask)
// router.get('/:user_id/tasks/:task_id/destroy', CourseRoutes.deleteTask)

module.exports = router
