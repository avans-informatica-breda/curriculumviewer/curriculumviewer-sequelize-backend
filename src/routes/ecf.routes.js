const express = require('express')
const router = express.Router()
const EcfController = require('../controllers/ecf.controller')

router.get('/', EcfController.getAll)
// router.get('/:areaTag/:competenceLevel/:proficiencyLevel', EcfController.getOne)

module.exports = router
