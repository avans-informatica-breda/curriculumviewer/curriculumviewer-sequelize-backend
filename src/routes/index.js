const express = require('express')
const router = express.Router()

const boksRoutes = require('./boks.routes')
const courseRoutes = require('./course.routes')
const moduleRoutes = require('./module.routes')
const personRoutes = require('./person.routes')
const objectiveRoutes = require('./objective.routes')
const ecfRoutes = require('./ecf.routes')

// Installing routes
router.use('/api/boksnodes', boksRoutes)
router.use('/api/courses', courseRoutes)
router.use('/api/modules', moduleRoutes)
router.use('/api/objectives', objectiveRoutes)
router.use('/api/persons', personRoutes)
router.use('/api/ecf', ecfRoutes)

module.exports = router
