const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')
const assert = require('assert')

const includedModels = [
  {
    model: models.Objective,
    as: 'objectives'
  },
  {
    model: models.BoksContent,
    as: 'boksContent'
  },
  {
    model: models.Person,
    as: 'responsiblePerson'
  },
  {
    model: models.Course,
    as: 'course'
  },
  {
    model: models.Person,
    as: 'lastUpdatedBy',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  }
]

module.exports = {
  create: (req, res, next) => {
    logger.trace('create')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t => models.Module.create(req.body, { transaction: t }))
      .then(result => {
        if (result && result.dataValues && result.dataValues.id) {
          logger.trace('getById')
          return models.Module.findByPk(result.dataValues.id, {
            include: includedModels
          })
        } else {
          next({
            title: 'Error',
            message: 'Could not create module',
            code: HttpStatus.UNPROCESSABLE_ENTITY,
            date: new Date()
          })
        }
      })
      .then(result => res.status(HttpStatus.OK).json(result))
      .catch(error => {
        logger.error(error.toString())
        const apiError = {
          title: 'Error',
          message: 'Error creating module. A database constraint failed.',
          info: {
            sqlError: error.errors
              ? error.errors.map(item => item.message).reduce((a, b) => a + ' ' + b)
              : error.toString()
          },
          code: HttpStatus.UNPROCESSABLE_ENTITY,
          date: new Date()
        }
        next(apiError)
      })
  },

  getAll: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getAll - year = ${req.query.year}`)

    models.Module.findAll({
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => res.status(HttpStatus.OK).json(result))
      .catch(error => {
        logger.error('getAll module error: ', error.toString())
        next(error)
      })
  },

  getById: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getById - year = ${req.query.year}`)

    models.Module.findByPk(req.params.moduleId, {
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        if (result) {
          logger.trace('getById success')
          res.status(HttpStatus.OK).json(result)
        } else {
          next({
            message: 'Item not found',
            code: 404
          })
        }
      })
      .catch(error => {
        logger.error('getById module error: ', error.toString())
        next(error)
      })
  },

  updateById: (req, res, next) => {
    logger.trace(`updateById: update module id=${req.params.moduleId}`)

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t =>
        models.Module.update(req.body, {
          returning: false,
          where: {
            id: req.params.moduleId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(rowsUpdated => {
        logger.trace(`updateById: ${rowsUpdated} rows updated`)
        return models.Module.findByPk(req.params.moduleId, {
          include: includedModels
        })
      })
      .then(module => res.status(HttpStatus.OK).json(module))
      .catch(error => {
        logger.error('updateById error: ', error.toString())
        next(error)
      })
  },

  deleteById: (req, res, next) => {
    logger.trace('delete')

    models.sequelize
      .transaction(t =>
        models.Module.destroy({
          returning: false,
          where: {
            id: req.params.moduleId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(response => res.status(HttpStatus.OK).json(response))
      .catch(error => {
        logger.error('delete error: ', error.toString())
        next(error)
      })
  }
}
