const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')

module.exports = {
  //
  //
  //
  getAll: (req, res, next) => {
    logger.trace('getAll')
    models.Ecf.findAll()
      .then(result => {
        logger.trace('getAll success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('getAll error: ', error.toString())
        next(error)
      })
  }
}
