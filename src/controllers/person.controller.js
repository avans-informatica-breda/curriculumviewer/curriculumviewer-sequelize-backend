const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')

const includedModels = [
  {
    model: models.Course,
    as: 'responsibleForCourse'
  },
  {
    model: models.Module,
    as: 'responsibleForModules'
  },
  {
    model: models.Person,
    as: 'lastUpdatedBy',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  }
]

module.exports = {
  create: (req, res, next) => {
    logger.trace('create - NOT using audit log')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.Person.create(req.body)
      .then(result => {
        logger.trace('create success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('create error: ', error.toString())
        next(error)
      })
  },

  getAll: (req, res, next) => {
    logger.trace('getAll')
    models.Person.findAll({
      include: includedModels
    })
      .then(result => {
        logger.trace('getAll success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('getAll error: ', error.toString())
        next(error)
      })
  },

  getById: (req, res, next) => {
    logger.trace('getById')
    models.Person.findByPk(req.params.personId, {
      include: includedModels
    })
      .then(result => {
        if (result) {
          logger.trace('getById success')
          res.status(HttpStatus.OK).json(result)
        } else {
          next({
            message: 'Item not found',
            code: 404
          })
        }
      })
      .catch(error => {
        logger.error('getById error: ', error.toString())
        next(error)
      })
  },

  updateById: (req, res, next) => {
    logger.trace('updateById - NOT using audit log')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.Person.findById({
      id: req.params.personId
    })
      .then(result => {
        logger.trace('updateById success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('updateById error: ', error.toString())
        next(error)
      })
  },

  deleteById: (req, res, next) => {
    logger.trace('delete - NOT using audit log')
    models.Person.destroy({
      where: {
        id: req.params.personId
      }
    })
      .then(response => {
        logger.trace('delete success', response)
        res.redirect('/')
      })
      .catch(error => {
        logger.error('delete error: ', error.toString())
        next(error)
      })
  }
}
