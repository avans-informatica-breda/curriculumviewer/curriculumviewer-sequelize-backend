const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')

module.exports = {
  /**
   * Create a contentnode below an existing node. The parent node must exist.
   */
  createContentNode: (req, res, next) => {
    logger.trace('create contentnode below ', req.params.boksnodeId)
    req.body['parentId'] = req.params.boksnodeId

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t => models.BoksContent.create(req.body, { transaction: t, individualHooks: true }))
      .then(result => {
        logger.trace('create contentnode success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('create contentnode error: ', error.toString())
        next(error)
      })
  },

  updateById: (req, res, next) => {
    logger.trace(`updateById bokscontent ${req.params.bokscontentId}`)

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t =>
        models.BoksContent.update(req.body, {
          where: {
            id: req.params.bokscontentId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(result => {
        logger.trace(`updateById bokscontent ${req.params.bokscontentId} success`)
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error(`updateById bokscontent ${req.params.bokscontentId} error`, error.toString())
        next(error)
      })
  },

  deleteById: (req, res, next) => {
    logger.trace('delete')

    models.sequelize
      .transaction(t =>
        models.BoksContent.destroy({
          where: {
            id: req.params.bokscontentId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(response => {
        logger.trace(`deleted ${response} items`)
        response === 0
          ? res.status(HttpStatus.NOT_FOUND).json(response)
          : res.status(HttpStatus.OK).json(response)
      })
      .catch(error => {
        logger.error('delete error: ', error.toString())
        next(error)
      })
  }
}
