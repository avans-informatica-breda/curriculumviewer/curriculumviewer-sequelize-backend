const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')
const assert = require('assert')

const includedModels = [
  {
    model: models.Module,
    as: 'module'
  },
  {
    model: models.BoksNode,
    as: 'learningLines'
  },
  {
    model: models.Ecf,
    as: 'endqualifications'
  },
  {
    model: models.Person,
    as: 'lastUpdatedBy',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  }
]

module.exports = {
  //
  //
  //
  create: (req, res, next) => {
    logger.trace('create')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    const objective = models.Objective.build(req.body)

    models.sequelize
      .transaction(t =>
        objective
          .save({ transaction: t, individualHooks: true })
          .then(() =>
            objective.setLearningLines(req.body.learningLines, {
              through: {
                year: req.body.year,
                lastUpdatedById: req.body.lastUpdatedById
              },
              transaction: t,
              individualHooks: true
            })
          )
          .then(() =>
            objective.setEndqualifications(req.body.endqualifications, {
              through: {
                year: req.body.year,
                lastUpdatedById: req.body.lastUpdatedById
              },
              transaction: t,
              individualHooks: true
            })
          )
          .catch(error => {
            throw error
          })
      )
      .then(() =>
        objective.reload({
          include: includedModels
        })
      )
      .then(reloadedInstance => {
        res.status(HttpStatus.OK).json(reloadedInstance)
      })
      .catch(error => {
        logger.error(error.toString())
        const apiError = {
          title: 'Error',
          message: 'Error creating objective. One or more database constraints failed.',
          info: {
            sqlError: error.errors
              ? error.errors.map(item => item.message).reduce((a, b) => a + ' ' + b)
              : error.toString()
          },
          code: HttpStatus.UNPROCESSABLE_ENTITY,
          date: new Date()
        }
        next(apiError)
      })
  },

  //
  //
  //
  getAll: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getAll - year = ${req.query.year}`)

    models.Objective.findAll({
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        logger.trace('getAll success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('getAll error: ', error.toString())
        next(error)
      })
  },

  //
  //
  //
  getById: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getById - year = ${req.query.year}`)

    models.Objective.findByPk(req.params.objectiveId, {
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        if (result) {
          logger.trace('getById success')
          res.status(HttpStatus.OK).json(result)
        } else {
          next({
            message: 'Item not found',
            code: 404
          })
        }
      })
      .catch(error => {
        logger.error('getById error: ', error.toString())
        next(error)
      })
  },

  //
  //
  //
  updateById: (req, res, next) => {
    logger.trace(`updateById: id = ${req.params.objectiveId}`)

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    console.dir(req.body)

    models.Objective.findByPk(req.params.objectiveId)
      .then(objective =>
        models.sequelize
          .transaction(t =>
            objective
              .update(req.body, {
                returning: true,
                where: {
                  id: req.params.objectiveId
                },
                transaction: t,
                individualHooks: true
              })
              .then(() =>
                objective.setLearningLines(req.body.learningLines, {
                  through: {
                    year: req.body.year,
                    lastUpdatedById: req.body.lastUpdatedById
                  },
                  transaction: t,
                  individualHooks: true
                })
              )
              .then(() =>
                objective.setEndqualifications(req.body.endqualifications, {
                  through: {
                    year: req.body.year,
                    lastUpdatedById: req.body.lastUpdatedById
                  },
                  transaction: t,
                  individualHooks: true
                })
              )
          )
          .then(() =>
            objective.reload({
              include: includedModels
            })
          )
          .catch(error => {
            throw error
          })
      )
      // .then(() =>
      //   models.Objective.findByPk(req.params.objectiveId, {
      //     include: includedModels
      //   })
      // )
      .then(reloadedInstance => {
        res.status(HttpStatus.OK).json(reloadedInstance)
      })
      .catch(error => {
        logger.error(error.toString())
        const apiError = {
          title: 'Error',
          message: 'Error updating objective. One or more database constraints failed.',
          info: {
            sqlError: error.errors
              ? error.errors.map(item => item.message).reduce((a, b) => a + ' ' + b)
              : error.toString()
          },
          code: HttpStatus.UNPROCESSABLE_ENTITY,
          date: new Date()
        }
        next(apiError)
      })
  },

  //
  //
  //
  deleteById: (req, res, next) => {
    logger.trace('delete')

    models.sequelize
      .transaction(t =>
        models.Objective.destroy({
          where: {
            id: req.params.objectiveId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(response => {
        logger.trace('delete success', response)
        res.status(HttpStatus.OK).json(response)
      })
      .catch(error => {
        logger.error('delete error: ', error.toString())
        next(error)
      })
  }
}
