const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')
const assert = require('assert')

const includedModels = [
  {
    model: models.BoksNode,
    as: 'childNodes'
  },
  {
    model: models.BoksContent,
    as: 'contentNodes'
  },
  {
    model: models.Person,
    as: 'lastUpdatedBy',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  }
]

module.exports = {
  create: (req, res, next) => {
    logger.trace('create boksnode')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t => models.BoksNode.create(req.body, { transaction: t, individualHooks: true }))
      .then(result => {
        logger.trace('create boksnode success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('create boksnode error: ', error.toString())
        next(error)
      })
  },

  /**
   * Create a contentnode below an existing node. The parent node must exist.
   */
  createSubNode: (req, res, next) => {
    logger.trace('create boksnode below ', req.params.boksnodeId)
    req.body['parentId'] = req.params.boksnodeId

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.BoksNode.findByPk(req.params.boksnodeId)
      .then(result => {
        if (result === null) {
          logger.warn(`create boksnode - parent ${req.params.boksnodeId} does not exist`)
          return res.status(HttpStatus.OK).json({ response: 'Parent not found' })
        } else {
          models.sequelize
            .transaction(t => models.BoksNode.create(req.body, { transaction: t, individualHooks: true }))
            .then(result => {
              logger.trace('create boksnode success')
              res.status(HttpStatus.OK).json(result)
            })
            .catch(error => {
              logger.error('create boksnode error: ', error.toString())
              next(error)
            })
        }
      })
      .catch(error => {
        logger.error('create boksnode error: ', error.toString())
        next(error)
      })
  },

  getAll: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getAll - year = ${req.query.year}`)

    models.BoksNode.findAll({
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        logger.trace('getAll boksnodes success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('getAll boksnodes error: ', error.toString())
        next(error)
      })
  },

  getById: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }
    logger.trace(`getById - year = ${req.query.year}`)

    models.BoksNode.findByPk(req.params.boksnodeId, {
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        if (result) {
          logger.trace('getById success')
          res.status(HttpStatus.OK).json(result)
        } else {
          next({
            message: 'Item not found',
            code: 404
          })
        }
      })
      .catch(error => {
        logger.error('getById boksnode error: ', error.toString())
        next(error)
      })
  },

  updateById: (req, res, next) => {
    logger.trace('updateById boksnode', req.params.boksnodeId)

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t =>
        models.BoksNode.update(req.body, {
          where: {
            id: req.params.boksnodeId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(result => {
        logger.trace(`updateById boksnode ${req.params.boksnodeId} updated, result = ${result}`)
        return models.BoksNode.findByPk(req.params.boksnodeId, {
          include: includedModels
        })
      })
      .then(result => {
        logger.trace(`updateById boksnode ${req.params.boksnodeId} returning result = ${result}`)
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('updateById boksnode error: ', error.toString())
        next(error)
      })
  },

  deleteById: (req, res, next) => {
    logger.trace('delete boksnode')

    models.sequelize
      .transaction(t =>
        models.BoksNode.destroy({
          where: {
            id: req.params.boksnodeId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(response => {
        logger.trace(`delete boksnode: deleted ${response} items`)
        response === 0
          ? res.status(HttpStatus.NOT_FOUND).json(response)
          : res.status(HttpStatus.OK).json(response)
      })
      .catch(error => {
        logger.error('delete boksnode error: ', error.toString())
        next(error)
      })
  }
}
