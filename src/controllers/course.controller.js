const models = require('../models')
const logger = require('../config/config').logger
const HttpStatus = require('http-status')
const assert = require('assert')

const includedModels = [
  {
    model: models.Module,
    as: 'modules'
  },
  {
    model: models.Person,
    as: 'responsiblePerson',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  },
  {
    model: models.Person,
    as: 'lastUpdatedBy',
    attributes: { exclude: 'createdAt, updatedAt, emailAddress, isEditable' }
  }
]

module.exports = {
  create: (req, res, next) => {
    logger.trace('create')

    // Random lastUpdated user, until we have authentication
    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t => models.Course.create(req.body, { transaction: t, individualHooks: true }))
      .then(result => {
        if (result && result.dataValues && result.dataValues.id) {
          logger.trace('getById')
          return models.Course.findByPk(result.dataValues.id, {
            include: includedModels
          })
        } else {
          next({
            title: 'Error',
            message: 'Could not create course',
            code: HttpStatus.UNPROCESSABLE_ENTITY,
            date: new Date()
          })
        }
      })
      .then(result => {
        logger.trace('returning ')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('error creating record')
        if (error && error.errors) {
          const message = error.errors.map(item => item.message).reduce((a, b) => a + ' ' + b)
          const errorObject = {
            title: 'Error',
            message: message,
            code: HttpStatus.UNPROCESSABLE_ENTITY,
            date: new Date()
          }
          logger.error(errorObject)
          next(errorObject)
        } else {
          logger.error(error.toString())
          next(error)
        }
      })
  },

  /**
   * Get all rows for the given year.
   */
  getAll: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }

    logger.trace(`getAll - year = ${req.query.year}`)
    models.Course.findAll({
      where: {
        year: req.query.year
      },
      include: includedModels,
      order: [['studyYear', 'ASC'], ['trimester', 'ASC']]
      // attributes: ['id', 'name', 'createdAt']
    })
      .then(result => {
        logger.trace('getAll success')
        res.status(HttpStatus.OK).json(result)
      })
      .catch(error => {
        logger.error('getAll error: ', error.toString())
        next(error)
      })
  },

  getById: (req, res, next) => {
    try {
      assert(req.query.year, 'year is a required query param.')
    } catch (ex) {
      const errMsg = ex.toString()
      logger.warn(errMsg)
      return next({
        message: errMsg.substring(errMsg.indexOf(':') + 2, errMsg.length),
        code: 400
      })
    }

    logger.trace('getById - year =', req.query.year)
    models.Course.findByPk(req.params.courseId, {
      where: {
        year: req.query.year
      },
      include: includedModels
    })
      .then(result => {
        if (result) {
          logger.trace('getById success')
          res.status(HttpStatus.OK).json(result)
        } else {
          next({
            message: 'Item not found',
            code: 404
          })
        }
      })
      .catch(error => {
        logger.error('getById error: ', error.toString())
        next(error)
      })
  },

  updateById: (req, res, next) => {
    logger.trace(`updateById: update id=${req.params.courseId}`)

    req.body.lastUpdatedById = Math.floor(Math.random() * 3) + 1

    models.sequelize
      .transaction(t =>
        models.Course.update(req.body, {
          returning: false,
          where: {
            id: req.params.courseId
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(rowsUpdated => {
        logger.trace(`updateById: ${rowsUpdated} rows updated`)
        return models.Course.findByPk(req.params.courseId, {
          include: includedModels
        })
      })
      .then(module => res.status(HttpStatus.OK).json(module))
      .catch(error => {
        logger.error('updateById error: ', error.toString())
        next(error)
      })
  },

  deleteById: (req, res, next) => {
    logger.trace('delete')

    models.sequelize
      .transaction(t =>
        models.Course.destroy({
          where: {
            id: req.params.courseId,
            year: req.query.year
          },
          transaction: t,
          individualHooks: true
        })
      )
      .then(response => {
        logger.trace('delete success', response)
        res.redirect('/')
      })
      .catch(error => {
        logger.error('delete error: ', error.toString())
        next(error)
      })
  }
}
