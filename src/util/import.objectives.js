const models = require('../models')
const logger = require('../config/config').logger
const fs = require('fs')
const csv = require('csv')

const filename = './objectives-export.csv'
const YEAR = 2018
let ecfs
let boksNodes

models.Objective.destroy({
  where: {
    year: YEAR
  },
  restartIdentity: true
})
  .then(destroyedRows => {
    logger.info(`Destroyed ${destroyedRows} rows.`)
    return models.Ecf.findAll({ raw: true })
  })
  .then(result => {
    ecfs = result.map(item => {
      return {
        id: item.id,
        name: '' + item.areaTag.toLowerCase() + item.competenceLevel + item.proficiencyLevel
      }
    })
    logger.info(`Found ${ecfs.length} eCfs.`)
    console.dir(ecfs)
    return models.BoksNode.findAll({ where: { year: YEAR, isLearningLine: true }, raw: true })
  })
  .then(result => {
    boksNodes = result.map(item => {
      return {
        id: item.id,
        name: item.name
      }
    })
    logger.info(`Found ${boksNodes.length} learning lines.`)

    logger.info(`Reading file '${filename}'`)
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) {
        logger.warn('Unable to read', filename, err.toString())
      }
      csv.parse(
        data,
        {
          trim: true,
          delimiter: ';',
          skip_empty_lines: true
        },
        (err, data) => {
          if (err) {
            logger.error('Unable to read', filename, err.toString())
          }
          logger.info(`Transforming ${data.length} lines.`)
          csv.transform(data, data => {
            // logger.info('Transforming', data[2].substr(0, 50), '...')
            //
            //
            //
            models.Module.findAll({
              where: {
                year: 2018,
                name: data[1]
              }
            })
              .then(module => {
                if (module && module[0]) {
                  logger.info(`id: ${module[0].id} name: ${module[0].name}`)

                  // Random lastUpdated user, until we have authentication
                  const toCreate = {
                    year: data[0],
                    lastUpdatedById: Math.floor(Math.random() * 3) + 1,
                    moduleId: module[0].id,
                    name: data[2],
                    shortDescription: '',
                    bloomTaxonomy: data[3] && data[3].length > 0 ? data[3] : 'Toepassen'
                  }
                  const learningLines = boksNodes
                    .filter(
                      item =>
                        item.name.toLowerCase() === data[4].toLowerCase() ||
                        item.name.toLowerCase() === data[5].toLowerCase() ||
                        item.name.toLowerCase() === data[6].toLowerCase()
                    )
                    .map(item => item.id)
                  logger.info(`leerlijnen: ${data[4]} ${data[5]} ${data[6]} => '${learningLines}'`)

                  const qualification1 = '' + data[7] + data[8] + data[9]
                  const qualification2 = '' + data[10] + data[11] + data[12]
                  const qualification3 = '' + data[13] + data[14] + data[15]
                  const endqualifications = ecfs
                    .filter(
                      item =>
                        item.name.toLowerCase() === qualification1.toLowerCase() ||
                        item.name.toLowerCase() === qualification2.toLowerCase() ||
                        item.name.toLowerCase() === qualification3.toLowerCase()
                    )
                    .map(item => item.id)
                  logger.info(
                    `endquals: '${qualification1}' '${qualification2}' '${qualification3}' => '${endqualifications}'`
                  )

                  const objective = models.Objective.build(toCreate)
                  console.log({ ...toCreate, ll: learningLines, ecf: endqualifications })

                  // return Promise.reject('Hier stopt het nu')

                  models.sequelize
                    .transaction(t =>
                      objective
                        .save({ transaction: t, individualHooks: true })
                        .then(() =>
                          objective.setLearningLines(learningLines, {
                            through: {
                              year: toCreate.year,
                              lastUpdatedById: toCreate.lastUpdatedById
                            },
                            transaction: t,
                            individualHooks: true
                          })
                        )
                        .then(() =>
                          objective.setEndqualifications(endqualifications, {
                            through: {
                              year: toCreate.year,
                              lastUpdatedById: toCreate.lastUpdatedById
                            },
                            transaction: t,
                            individualHooks: true
                          })
                        )
                        .then(result => logger.info(`Created`))
                        .catch(err => logger.error(err.toString()))
                    )
                    .catch(error => {
                      logger.error(error.toString())
                    })
                } else {
                  logger.warn(`Module '${data[1]}' niet gevonden, geen leerdoel gekoppeld!`)
                }
              })
              .catch(err => {
                logger.error(err.toString())
                models.sequelize.connectionManager.close().then(() => console.log('shut down gracefully'))
              })

            //
            //
            //
          })
        }
      )
    })
  })
  .then()
  .catch(err => {
    logger.error(err.toString())
    models.sequelize.connectionManager.close().then(() => console.log('shut down gracefully'))
  })
