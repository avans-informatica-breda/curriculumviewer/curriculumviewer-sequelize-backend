// Set the logging level.
const loglevel = process.env.LOGLEVEL || 'trace'

module.exports = {
  development: {
    username: 'inzicht_user',
    password: 'secret',
    database: 'inzicht',
    host: 'localhost',
    port: 3306,
    dialect: 'mysql',
    logging: false,
    use_env_variable: false,
    define: {
      timestamps: true,
      paranoid: false
    }
  },
  test: {
    dialect: 'sqlite',
    logging: false,
    storage: ':memory:'
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DBNAME,
    host: process.env.DB_HOSTNAME,
    port: 3306,
    dialect: 'mysql',
    // dialectOptions: {
    // Niet nodig omdat we de dtb server in Docker draaien
    //   socketPath: '/var/run/mysqld/mysqld.sock'
    // },
    logging: false, // console.log,
    use_env_variable: false,
    define: {
      timestamps: true
    }
  },

  logger: require('tracer').console({
    format: ['{{timestamp}} <{{title}}> {{file}}:{{line}} : {{message}}'],
    preprocess: function(data) {
      data.title = data.title.toUpperCase()
    },
    dateformat: 'isoUtcDateTime',
    level: loglevel
  })
}
