# Avans Hogeschool Breda - curriculum viewer server

This repository demonstrates the usage of Sequelize within an [Express](https://expressjs.com) application.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

## Starting App

**Without Migrations**

```
npm install
npm start
```

**With Migrations**

```
npm install
.\node_modules\.bin\sequelize db:migrate
npm start
```

This will start the application and create an sqlite database in your app dir.
Just open [http://localhost:3000](http://localhost:3000).

**Undo Migrations**

```
.\node_modules\.bin\sequelize db:migrate:undo
```

## Running Tests

We have added some [Mocha](https://mochajs.org) based test. You can run them by `npm test`

## Prettyfying code

Run

```
npx pretty-quick
```
