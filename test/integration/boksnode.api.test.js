'use strict'

const app = require('../../src/app')
const assert = require('assert')
const Bluebird = require('bluebird')
const request = require('supertest')
const chai = require('chai')
const models = require('../../src/models')

const should = chai.should()

const endpoint = '/api/boksnodes'

const user = {
  id: 1,
  firstName: 'first',
  lastName: 'last',
  lastUpdatedById: 1,
  emailAdress: 'test@server.com'
}

describe(endpoint, function() {
  before(function() {
    return models.sequelize
      .sync()
      .then(() =>
        models.sequelize.transaction(t =>
          models.Person.create(user, { transaction: t, individualHooks: true })
        )
      )
  })

  beforeEach(function() {
    return Bluebird.all([
      models.BoksNode.destroy({ truncate: true }),
      models.BoksContent.destroy({ truncate: true })
    ])
  })

  it('should error when year is missing', function(done) {
    request(app)
      .get(endpoint)
      .end((err, res) => {
        assert(!err)
        res.statusCode.should.equal(400)
        done()
      })
  })

  it('should return an empty list of boks nodes after init', function(done) {
    request(app)
      .get(endpoint + '?year=2019')
      .end((err, res) => {
        assert(!err)
        res.statusCode.should.equal(200)
        res.body.should.be.an('array').that.has.length(0)
        done()
      })
  })

  it('should list the selected boksnode', function(done) {
    const boksNode = {
      name: 'test123',
      shortDescription: 'Het bokselement',
      administrativeRemark: 'test',
      childNodes: [],
      contentNodes: [],
      isLearningLine: true,
      longDescription: '',
      lastUpdatedById: 1,
      parentId: null,
      year: 2016
    }

    models.sequelize
      .transaction(t => models.BoksNode.create(boksNode, { transaction: t, individualHooks: true }))
      .then(() => {
        request(app)
          .get(endpoint + '?year=' + boksNode.year)
          .end((err, res) => {
            assert(!err)
            res.statusCode.should.equal(200)
            res.body.should.be.an('array').that.has.length(1)

            const boksnode = res.body[0]
            boksnode.name.should.equal(boksNode.name)
            done()
          })
      })
      .catch(err => done(err))
  })
})
