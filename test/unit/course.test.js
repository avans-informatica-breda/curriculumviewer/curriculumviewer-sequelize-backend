'use strict'

const expect = require('expect.js')
const models = require('../../src/models')

const courseToCreate = {
  name: 'Periode 2',
  shortDescription: 'De tweede periode (course)',
  administrativeRemark: 'test',
  responsiblePersonId: 1,
  year: 2018,
  studyYear: 1,
  trimester: 2,
  lastUpdatedById: 1,
  form: 'Regulier'
}

const moduleToCreate = {
  name: 'Databases 1',
  shortDescription: 'Hier een beschrijving van deze module',
  longDescription: 'Hier eventueel een uitgebreidere toelichting.',
  form: 'Regulier',
  courseId: 1,
  credits: 4.0,
  lastUpdatedById: 1,
  responsiblePersonId: 2
}

const user = {
  id: 1,
  firstName: 'first',
  lastName: 'last',
  lastUpdatedById: 1,
  emailAdress: 'test@server.com'
}

describe('models/course', () => {
  before(() => {
    return models.sequelize
      .sync()
      .then(() =>
        models.sequelize.transaction(t =>
          models.Person.create(user, { transaction: t, individualHooks: true })
        )
      )
  })

  beforeEach(() => {
    this.Course = models.Course
    this.Module = models.Module
    this.Person = models.Person
  })

  describe('create', () => {
    it('creates entity', done => {
      models.sequelize
        .transaction(t => this.Course.create(courseToCreate, { transaction: t, individualHooks: true }))
        .bind(this)
        .then(course => {
          expect(course.name).to.equal(courseToCreate.name)
          courseToCreate.id = course.id
          done()
        })
        .catch(err => {
          done(err)
        })
    })
  })

  describe('destroy', () => {
    it('removes the previously created entity', done => {
      models.sequelize
        .transaction(t =>
          this.Course.destroy({
            where: {
              id: courseToCreate.id
            },
            transaction: t,
            individualHooks: true
          })
        )
        .bind(this)
        .then(result => {
          expect(result).to.equal(1)
          done()
        })
        .catch(err => {
          done(err)
        })
    })
  })
})
