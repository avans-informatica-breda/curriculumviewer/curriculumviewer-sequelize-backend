'use strict'

const expect = require('expect.js')

describe('models/index', function() {
  it('returns all created models', function() {
    const models = require('../../src/models')
    expect(models.Course).to.be.ok()
    expect(models.Module).to.be.ok()
    expect(models.Objective).to.be.ok()
    expect(models.Person).to.be.ok()
    expect(models.BoksNode).to.be.ok()
    expect(models.BoksContent).to.be.ok()
    expect(models.ObjectiveBoksNode).to.be.ok()
    expect(models.ObjectiveEcf).to.be.ok()
    expect(models.AuditLog).to.be.ok()
  })
})
