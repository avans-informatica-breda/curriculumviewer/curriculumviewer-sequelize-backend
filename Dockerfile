# Create image based on the official Node image from dockerhub
FROM node:latest

# Create a directory where our app will be placed
RUN mkdir -p /usr/src

# Change directory so that our commands run inside this new directory
WORKDIR /usr/src

# Copy all the code needed to run the application into the working directory
COPY . /usr/src

# Set environment variables
ENV NODE_ENV=production

# Install dependecies
RUN npm install --no-optional --unsafe-perm --registry=http://registry.npmjs.org

# Expose the port the app runs in
EXPOSE 3030

# Serve the app
CMD ["node", "src/server.js"]
