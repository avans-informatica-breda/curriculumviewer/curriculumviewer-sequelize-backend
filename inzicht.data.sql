-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: inzicht
-- ------------------------------------------------------

DELETE FROM `AuditLogs`;
ALTER TABLE `AuditLogs` AUTO_INCREMENT = 0;
DELETE FROM `BoksNodes`;
ALTER TABLE `BoksNodes` AUTO_INCREMENT = 0;
DELETE FROM `BoksContents`;
ALTER TABLE `BoksContents` AUTO_INCREMENT = 0;
DELETE FROM `Objectives`;
ALTER TABLE `Objectives` AUTO_INCREMENT = 0;
DELETE FROM `ObjectiveBoksNodes`;
ALTER TABLE `ObjectiveBoksNodes` AUTO_INCREMENT = 0;
DELETE FROM `ObjectiveEcfs`;
ALTER TABLE `ObjectiveEcfs` AUTO_INCREMENT = 0;
DELETE FROM `Courses`;
ALTER TABLE `Courses` AUTO_INCREMENT = 0;
DELETE FROM `Modules`;
ALTER TABLE `Modules` AUTO_INCREMENT = 0;
DELETE FROM `People`;
DELETE FROM `Ecfs`;

SET @yes = 1;
SET @no = 0;
SET @createdAt = NOW();
SET @updatedAt = NOW();


--
-- Dumping data for table `People`
--
INSERT INTO `People` (`id`, `isEditable`, `lastUpdatedById`, `firstName`, `lastName`, `emailAdress`, `createdAt`, `updatedAt`) VALUES
(0, 1, @Admin, 'Admin', '', 'admin@avans.nl', @createdAt, @updatedAt);
SET @Admin = 0;

INSERT INTO `People` (`id`, `isEditable`, `lastUpdatedById`, `firstName`, `lastName`, `emailAdress`, `createdAt`, `updatedAt`) VALUES
(1, 1, @Admin, 'Ruud', 'Hermans', 'user@avans.nl', @createdAt, @updatedAt);
SET @Ruud = 1;

INSERT INTO `People` VALUES
(2, 1, @Admin, 'Erco', 'Argante', 'user@avans.nl', @createdAt, @updatedAt);
SET @Erco = 2;

INSERT INTO `People` VALUES
(3, 1, @Admin, 'Arno', 'Broeders', 'user@avans.nl', @createdAt, @updatedAt);
SET @Arno = 3;

INSERT INTO `People` VALUES

(4, 1, @Admin, 'Eefje', 'Gijzen', 'user@avans.nl', @createdAt, @updatedAt);
SET @Eefje = 4;

INSERT INTO `People` VALUES
(5, 1, @Admin, 'Gitta', 'De Vaan', 'user@avans.nl', @createdAt, @updatedAt);
SET @Gitta = 5;

INSERT INTO `People` VALUES
(6, 1, @Admin, 'Qurratulain', 'Mubarak', 'user@avans.nl', @createdAt, @updatedAt);
SET @Qurratulain = 6;

INSERT INTO `People` VALUES
(7, 1, @Admin, 'Peter', 'Vos', 'user@avans.nl', @createdAt, @updatedAt);
SET @Peter = 7;

INSERT INTO `People` VALUES
(8, 1, @Admin, 'Johan', 'Smarius', 'user@avans.nl', @createdAt, @updatedAt);
SET @Johan = 8;

INSERT INTO `People` VALUES
(9, 1, @Admin, 'Pascal', 'Van Gastel', 'user@avans.nl', @createdAt, @updatedAt);
SET @Pascal = 9;

INSERT INTO `People` VALUES
(10, 1, @Admin, 'Gerard', 'Wagenaar', 'user@avans.nl', @createdAt, @updatedAt);
SET @Gerard = 10;

INSERT INTO `People` VALUES
(11, 1, @Admin, 'Ger', 'Oosting', 'user@avans.nl', @createdAt, @updatedAt);
SET @Ger = 11;

INSERT INTO `People` VALUES
(12, 1, @Admin, 'Jan', 'Montizaan', 'user@avans.nl', @createdAt, @updatedAt);
SET @Jan = 12;

INSERT INTO `People` VALUES
(13, 1, @Admin, 'Erik', 'Kuiper', 'user@avans.nl', @createdAt, @updatedAt);
SET @Erik = 13;

INSERT INTO `People` VALUES
(14, 1, @Admin, 'Dion', 'Koeze', 'user@avans.nl', @createdAt, @updatedAt);
SET @Dion = 14;

INSERT INTO `People` VALUES
(15, 1, @Admin, 'Alexander', 'Van den Bulck', 'user@avans.nl', @createdAt, @updatedAt);
SET @Alexander = 15;

INSERT INTO `People` VALUES
(16, 1, @Admin, 'Evert Jan', 'De Voogt', 'user@avans.nl', @createdAt, @updatedAt);
SET @EvertJan = 16;

INSERT INTO `People` VALUES
(17, 1, @Admin, 'Robin', 'Schellius', 'user@avans.nl', @createdAt, @updatedAt);
SET @Robin = 17;

INSERT INTO `People` VALUES
(18, 1, @Admin, 'Heidie', 'Tops', 'user@avans.nl', @createdAt, @updatedAt);
SET @Heidie = 18;


--
-- Boksnodes
--
INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Programmeren', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @Programmeren = LAST_INSERT_ID();
INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'OO Programmeren', @Programmeren, @Admin, 'Korte beschrijving', @no, @createdAt, @updatedAt),
(2018, 'Functioneel Programmeren', @Programmeren, @Admin, 'Korte beschrijving', @no, @createdAt, @updatedAt);

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Databases', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @Databases = LAST_INSERT_ID();
INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Relationele Databases', @Databases, @Admin, 'Korte beschrijving', @no, @createdAt, @updatedAt),
(2018, 'NoSql Databases', @Databases, @Admin, 'Korte beschrijving', @no, @createdAt, @updatedAt);

--
--  Vanaf hier BOKS nog aanvullen.
--
INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Bedrijfsprocessen', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @Bedrijfsprocessen = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Duurzame ontwikkeling', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @DuurzameOntwikkeling = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'HCI', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @HCI = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Onderzoeksvaardigheden', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @Onderzoeksvaardigheden = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Professionele vaardigheden', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @ProfessioneleVaardigheden = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Requirements Engineering', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @RequirementsEngineering = LAST_INSERT_ID();

INSERT INTO `BoksNodes` (`year`, `name`, `parentId`, `lastUpdatedById`, `shortDescription`, `isLearningLine`, `createdAt`, `updatedAt`) VALUES
(2018, 'Testen', NULL, @Admin, 'Korte beschrijving', @yes, @createdAt, @updatedAt);
SET @Testen = LAST_INSERT_ID();

INSERT INTO `Ecfs`
VALUES
(1, 1, 'A', 'plan', 5, 'Architecture Design', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(2, 1, 'A', 'plan', 5, 'Architecture Design', 2, 'Sample proficiency description', @createdAt, @updatedAt),
(3, 1, 'A', 'plan', 5, 'Architecture Design', 3, 'Sample proficiency description', @createdAt, @updatedAt),
(4, 1, 'A', 'plan', 6, 'Application Design', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(5, 1, 'A', 'plan', 8, 'Sustainable Development', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(6, 1, 'B', 'build', 1, 'Application Development', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(7, 1, 'B', 'build', 2, 'Component Integration', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(8, 1, 'B', 'build', 3, 'Testing', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(9, 1, 'B', 'build', 4, 'Solution Deployment', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(10, 1, 'B', 'build', 5, 'Documentation Production', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(11, 1, 'B', 'build', 6, 'Systems Engineering', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(12, 1, 'C', 'run', 4, 'Problem Management', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(13, 1, 'D', 'enable', 11, 'Needs Identification', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(14, 1, 'E', 'manage', 2, 'Project and Portfolio Management', 1, 'Sample proficiency description', @createdAt, @updatedAt),
(15, 1, 'F', 'professional skills', 1, 'Communicatieve Vaardigheden', 1, 'Rapportagevaardigheden', @createdAt, @updatedAt),
(16, 1, 'F', 'professional skills', 1, 'Communicatieve Vaardigheden', 2, 'Presentatievaardigheden', @createdAt, @updatedAt),
(17, 1, 'F', 'professional skills', 1, 'Communicatieve Vaardigheden', 3, 'Mondelinge communicatie', @createdAt, @updatedAt),
(18, 1, 'F', 'professional skills', 2, 'Leervaardigheden', 1, 'Kritische zelfreflectie', @createdAt, @updatedAt),
(19, 1, 'F', 'professional skills', 2, 'Leervaardigheden', 2, 'Evalueren van eigen resultaten', @createdAt, @updatedAt),
(20, 1, 'F', 'professional skills', 2, 'Leervaardigheden', 3, 'Nieuwe kennis/vaardigheden opdoen', @createdAt, @updatedAt),
(21, 1, 'F', 'professional skills', 3, 'Oordeelsvorming & Onderzoekend Vermogen', 1, 'Methodisch beantwoorden projectvragen + verantwoording keuzes + methodische aanpak', @createdAt, @updatedAt),
(22, 1, 'F', 'professional skills', 3, 'Oordeelsvorming & Onderzoekend Vermogen', 2, 'Onderzoekend vermogen en kritische houding', @createdAt, @updatedAt),
(23, 1, 'F', 'professional skills', 3, 'Oordeelsvorming & Onderzoekend Vermogen', 3, 'Analytisch vermogen + oordeelsvorming', @createdAt, @updatedAt),
(24, 1, 'F', 'professional skills', 4, 'Samenwerken & Omgevingsbewustzijn', 1, 'Samenwerken', @createdAt, @updatedAt),
(25, 1, 'F', 'professional skills', 4, 'Samenwerken & Omgevingsbewustzijn', 2, 'Omgevingsbewustzijn', @createdAt, @updatedAt),
(26, 1, 'F', 'professional skills', 4, 'Samenwerken & Omgevingsbewustzijn', 3, 'Multidisciplinariteit', @createdAt, @updatedAt),
(27, 1, 'F', 'professional skills', 4, 'Samenwerken & Omgevingsbewustzijn', 4, 'Internationalisering', @createdAt, @updatedAt);



INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `shortDescription`, `studyYear`, `trimester`, `form`, `responsiblePersonId`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Basisvaardigheden 1', 'Dit is de eerste periode van het eerste jaar', 1, 1, 'Regulier', @ruud, @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();

INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `shortDescription`, `credits`, `courseId`, `form`, `responsiblePersonId`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Beroepsoriëntatie', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @EvertJan, @createdAt, @updatedAt),
(2018, @Admin, 'Studievaardigheden', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @EvertJan, @createdAt, @updatedAt),
(2018, @Admin, 'Nederlands', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Gitta, @createdAt, @updatedAt),
(2018, @Admin, 'Bedrijfsprocessen 1', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Gerard, @createdAt, @updatedAt),
(2018, @Admin, 'Relationele Databases 1', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Alexander, @createdAt, @updatedAt),
(2018, @Admin, 'Webdesign', 'Dit is de onderwijseenheid Webdesign', 1.0, @courseId, 'Regulier', @Robin, @createdAt, @updatedAt),
(2018, @Admin, 'Programmeren 1', 'Dit is de onderwijseenheid Programmeren 1', 1.0, @courseId, 'Regulier', @Arno, @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `shortDescription`, `studyYear`, `trimester`, `form`, `responsiblePersonId`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Basisvaardigheden 2', 'Dit is de tweede periode van het eerste jaar', 1, 2, 'Regulier', @Johan, @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `shortDescription`, `credits`, `courseId`, `form`, `responsiblePersonId`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Requirements Engineering', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Gerard, @createdAt, @updatedAt),
(2018, @Admin, 'Relationele Databases 2', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Alexander, @createdAt, @updatedAt),
(2018, @Admin, 'Duurzame Ontwikkeling 1', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Eefje, @createdAt, @updatedAt),
(2018, @Admin, 'Professionele Vaardigheden 1', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @EvertJan, @createdAt, @updatedAt),
(2018, @Admin, 'Basic English', 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @Eefje, @createdAt, @updatedAt),
(2018, @Admin, 'Programmeren 2', 'Dit is de onderwijseenheid Programmeren 2', 4.0, @courseId, 'Regulier', @Arno, @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Basisvaardigheden 3', @Johan, 'Dit is de derde periode van het eerste jaar', 1, 3, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Softwareontwerp & Architectuur 1', @Jan, 'Dit is een van de onderwijseenheden ', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Relationele Databases 3', @Alexander, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Onderzoeksvaardigheden 1', @Gitta, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Projectmanagement & Software Engineering', @Jan, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Programmeren 3', @Robin, 'Dit is de onderwijseenheid Programmeren 2', 4.0, @courseId, 'Regulier', @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Toepassing Basisvaardigheden', @Erik, 'Dit is de vierde periode van het eerste jaar', 1, 4, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Professionele Vaardigheden 2', @Peter, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Computernetwerken 1', @Johan, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Project - iteratie 1', @Erik, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 2.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Project - iteratie 2', @Erik, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 2.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Project - iteratie 3', @Erik, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 4.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Programmeren 4', @Robin, 'Dit is de onderwijseenheid Programmeren 2', 4.0, @courseId, 'Regulier', @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Server-side web programming', @Johan, 'Dit is de eerste periode van het tweede jaar', 2, 1, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Server-side web programming individueel', @Johan, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Server-side web programming project', @Johan, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'UX Design', @EvertJan, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Algoritmen en Datastructuren', @Erco, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Softwareontwerp & Architectuur 2', @Jan, 'Dit is een van de onderwijseenheden ', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt)
;

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Client-side web programming', @Robin, 'Dit is de tweede periode van het tweede jaar', 2, 2, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Client-side web frameworks', @Robin, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Client-side web programming individueel', @Robin, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Client-side web programming project', @Robin, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'NoSql Databases', @Alexander, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt),
(2018, @Admin, 'Professionele Vaardigheden 3', @Gitta, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt)
;

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Data science', @Gerard, 'Dit is de derde periode van het tweede jaar', 2, 3, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Professionele Vaardigheden 1', @Peter, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt)


;

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Infrastructuur', @Arno, 'Dit is de vierde periode van het tweede jaar', 2, 4, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Professionele Vaardigheden 1', @Peter, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Stage', @Pascal, 'Dit is de stage', 3, 1, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Stage', @Pascal, 'Dit is de stage', 3, 2, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Softwarearchitectuur', @Arno, 'Dit is de derde periode van het derde jaar', 3, 3, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Professionele Vaardigheden 1', @Peter, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Solution Architecture', @Erco, 'Dit is de vierde periode van het derde jaar', 3, 4, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
INSERT INTO `Modules` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `credits`, `courseId`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Professionele Vaardigheden 1', @Peter, 'Dit is een van de onderwijseenheden uit het de eerste periode van het eerste jaar', 1.0, @courseId, 'Regulier', @createdAt, @updatedAt);

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Minor periode 1', @Erco, 'Dit is de eerste periode van het vierde jaar', 4, 1, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Minor periode 2', @Erco, 'Dit is de tweede periode van het vierde jaar', 4, 2, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();

INSERT INTO `Courses` (`year`, `lastUpdatedById`, `name`, `responsiblePersonId`, `shortDescription`, `studyYear`, `trimester`, `form`, `createdAt`, `updatedAt`) VALUES
(2018, @Admin, 'Afstuderen', @Ger, 'Afstuderen vindt plaats in de derde en vierde periode van het vierde jaar', 4, 3, 'Regulier', @createdAt, @updatedAt);
SET @courseId = LAST_INSERT_ID();
