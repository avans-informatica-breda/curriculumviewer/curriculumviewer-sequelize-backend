--
-- Create the databae and db user.
--
DROP DATABASE IF EXISTS `inzicht`;
DROP USER 'inzicht_user'@'localhost';
CREATE DATABASE `inzicht`;
USE `inzicht`;
CREATE USER 'inzicht_user'@'localhost' IDENTIFIED BY 'secret';
CREATE USER 'inzicht_user'@'%' IDENTIFIED BY 'secret';
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON `inzicht`.* TO 'inzicht_user'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON `inzicht`.* TO 'inzicht_user'@'%';
